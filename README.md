# oauth2.0授权登录

#### 介绍
简单的第三方登陆springboot项目demo,目前已经集成了gitee、githup和百度第三方登陆，后续计划集成
qq登陆、微信登陆以及微博登陆。

注：代码里面保留了我本人的gitee的client_id和密钥，pull下代码项目跑起来
就可以访问localhost:8080/oauth/gitee看到gitee的第三方登陆效果。

## 软件架构
项目基于springboot,通过oauth认证方式登陆。

## 使用说明

### 1.  申请账号

![oauth.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/oauth.png)
### 2.  将账号的clientId secret 填入, 拼接第三方登陆请求的url

![oauth2.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/ouath2.png)
![oauth2.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/oauth3.png)
![oauth2.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/oauth4.png)
需要注意的是，这里的redirect_uri回调地址需要和前面申请的账号里的应用回调地址完全一致。

### 3.  发起http请求调用api

###  最后贴一个效果图
#### 拼接授权uri,跳转至授权页

![oauth2.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/5.png)

#### 授权后回调本地返回json结果

![oauth2.png](https://lxin97.oss-cn-beijing.aliyuncs.com/blog/pic/6.png)

