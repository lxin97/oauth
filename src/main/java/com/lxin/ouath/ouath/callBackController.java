package com.lxin.ouath.ouath;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lxin.ouath.httpUtils.OkHttpUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class callBackController {

  //第三方gitee登陆回调
  @RequestMapping("/oauth/callback/gitee")
  @ResponseBody
  public String callback( String code) throws IOException {
    String client_id="326188d46ea12bf1facf797c2fbe6ffb6a82d36dedbc6109f2b9e364baaa4f8b";
    String redirect_uri="http://127.0.0.1:8080/oauth/callback/gitee";
    String client_secret="b567a6bba7fd0444ed8bcfba16a26fcac710dc097506df6ac8281ae2c6ad1caf";  //你申请的client_secret
    String url="https://gitee.com/oauth/token?grant_type=authorization_code"
        + "&code=" + code+
        "&client_id="+client_id +
        "&redirect_uri=" +redirect_uri+
        "&client_secret="+client_secret;
    String result= OkHttpUtils.post(url);
    System.out.println(result);
    JSON.toJSON(result);
    JSONObject jsonObject = JSONObject.parseObject(result);
    String token=jsonObject.getString("access_token");
    String getUserUrl="https://gitee.com/api/v5/user?access_token="+token;
    String userJson=OkHttpUtils.OkGetArt(getUserUrl);
    return userJson;
  }
  //第三方githup登陆回调
  @RequestMapping("/oauth/callback/githup")
  @ResponseBody
  public String callbackGitHup( String code) throws IOException {
    String client_id="****";
    String client_secret="****";
    String url="https://github.com/login/oauth/access_token?"
        + "code=" + code+
        "&client_id="+client_id +
        "&client_secret="+client_secret;
    String githupStr=OkHttpUtils.post(url);
    System.out.println(githupStr);
    String token = githupStr.substring(githupStr.indexOf("=")+1,githupStr.indexOf("&"));
    String getUserUrl="https://api.github.com/user";
    String userJson=OkHttpUtils.OkGetArt(getUserUrl,token);
    return userJson;
  }
  //第三方百度登陆回调
  @RequestMapping("/oauth/callback/baidu")
  @ResponseBody
  public String callbackBaiDu( String code) throws IOException {
    String client_id="****";
    String redirect_uri="http://127.0.0.1:8080/oauth/callback/baidu";
    String client_secret="****";
    String url="https://openapi.baidu.com/oauth/2.0/token?grant_type=authorization_code&"
        + "code=" + code+
        "&client_id="+client_id +
        "&client_secret="+client_secret+
        "&redirect_uri=" +redirect_uri+"";
    String githupStr=OkHttpUtils.post(url);
    System.out.println(githupStr);
    JSONObject jsonObject = JSONObject.parseObject(githupStr);
    String token = jsonObject.getString("access_token");
    String getUserUrl="https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser?access_token="+token;
    String userJson=OkHttpUtils.OkGetArt(getUserUrl);
    JSONObject userResult = JSONObject.parseObject(userJson);
    userResult.put("portrait","http://tb.himg.baidu.com/sys/portraitn/item/"+userResult.getString("portrait"));
    return userResult.toJSONString();
  }


}
