package com.lxin.ouath.ouath;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class oauthController {

    @RequestMapping("/oauth/{type}")
    public void oauth( @PathVariable String type,HttpServletResponse response) throws IOException {
        if ("gitee".equals(type)) {
            gitee(response);
        } else if ("githup".equals(type)) {
            githup(response);
        } else if ("baidu".equals(type)) {
            baidu(response);
        }
    }

    //第三方gitee登陆
    public void gitee(HttpServletResponse response) throws IOException {
        String client_id="326188d46ea12bf1facf797c2fbe6ffb6a82d36dedbc6109f2b9e364baaa4f8b";   //你申请的client_id
        String redirect_uri="http://127.0.0.1:8080/oauth/callback/gitee";
        String url="https://gitee.com/oauth/authorize?response_type=code&scope=user_info&client_id=";
        url=url+client_id+"&redirect_uri="+redirect_uri;
        response.sendRedirect(url);
    }
    //第三方githup登陆
    public void githup(HttpServletResponse response) throws IOException {
        String client_id="****";                                                //你申请的client_id
        String redirect_uri="http://127.0.0.1:8080/oauth/callback/githup";      //回调的地址
        String url="https://github.com/login/oauth/authorize?client_id=";       //githup授权的地址
        url=url+client_id+"&redirect_uri="+redirect_uri;
        response.sendRedirect(url);
    }
    //第三方百度登陆
    public void baidu(HttpServletResponse response) throws IOException {
        String client_id="****";                                                //你申请的client_id
        String redirect_uri="http://127.0.0.1:8080/oauth/callback/baidu";
        String url="https://openapi.baidu.com/oauth/2.0/authorize?response_type=code&client_id=";
        url=url+client_id+"&redirect_uri="+redirect_uri+"&scope=basic&display=popup";
        response.sendRedirect(url);
    }
}
