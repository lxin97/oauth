package com.lxin.ouath.httpUtils;

import okhttp3.*;

import java.io.IOException;

public class OkHttpUtils {
    public static String OkGetArt(String url){
        String html = null;
        OkHttpClient client = new OkHttpClient();
        Request requestObj = new Request.Builder().url(url).addHeader("Content-type","charset=utf-8").build();
        try (Response responseObj = client.newCall(requestObj).execute()){
            html = responseObj.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html;

    }
    public static String OkGetArt(String url,String token){
        String html = null;
        OkHttpClient client = new OkHttpClient();
        Request requestObj = new Request.Builder().url(url)
                .addHeader("Content-type","charset=utf-8")
                .addHeader("Authorization","token "+token).build();
        try (Response responseObj = client.newCall(requestObj).execute()){
            html = responseObj.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html;

    }
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    static OkHttpClient client = new OkHttpClient();
    public static String post(String url) throws IOException {
        RequestBody body = RequestBody.create(JSON, "");
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-type","charset=utf-8")
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            return response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
    }
}
